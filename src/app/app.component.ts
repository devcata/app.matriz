import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { DatabaseProvider } from '../providers/database/database';

import { LoginPage } from '../pages/login/login';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, storage: DatabaseProvider) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      storage.Create().then(async () =>{
        await storage.CreateTables();
        await storage.SetupDefault();
        this.OpenLoginPage(splashScreen);
      }).catch(error => console.log(JSON.stringify(error)));
      
    });
  }

  private OpenLoginPage(splashScreen: SplashScreen): void {
    splashScreen.hide();
    this.rootPage = LoginPage;
  }
}

