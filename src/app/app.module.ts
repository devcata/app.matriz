import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SQLite } from '@ionic-native/sqlite';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
import { SelectSearchableModule } from 'ionic-select-searchable';

import { MyApp } from './app.component';

import { DatabaseProvider } from '../providers/database/database';
import { RequestProvider } from '../providers/request/request';

import { EfficiencyPage } from '../pages/efficiency/efficiency';
import { UnproductivePage } from '../pages/unproductive/unproductive';
import { LoginPage } from '../pages/login/login';
import { MenuPage } from '../pages/menu/menu';
import { SetupPage } from '../pages/setup/setup';
import { CostsresourcesPage } from '../pages/costsresources/costsresources';
import { PointPage } from '../pages/point/point';
import { ShowefficiencyPage } from '../pages/showefficiency/showefficiency';
import { Unproductive1Page } from '../pages/unproductive1/unproductive1';
import { Unproductive2Page } from '../pages/unproductive2/unproductive2';
import { Unproductive3Page } from '../pages/unproductive3/unproductive3';


@NgModule({
  declarations: [
    EfficiencyPage,
    UnproductivePage,
    Unproductive1Page,
    Unproductive2Page,
    Unproductive3Page,
    LoginPage,
    MyApp,
    MenuPage,
    SetupPage,
    CostsresourcesPage,
    PointPage,
    ShowefficiencyPage
  ],
  imports: [
    SelectSearchableModule,
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    EfficiencyPage,
    UnproductivePage,
    Unproductive1Page,
    Unproductive2Page,
    Unproductive3Page,
    LoginPage,
    MyApp,
    MenuPage,
    SetupPage,
    CostsresourcesPage,
    PointPage,
    ShowefficiencyPage
  ],
  providers: [
    DatabaseProvider,
    Network,
    RequestProvider,
    StatusBar,
    SplashScreen,
    SQLite,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
