# Documentação de instalação do aplicativo de apontamento de eficência e improdutivo.  (CATA Matriz)

#### Configuração de ambiente de desenvolvimento para gerar teste ou build do aplicativo.

1. Ter instalado o Android Studio

2. Ter instalado o JDK do Java

3. Ter instalado o NodeJS 8.11.1 ou superior

4. Ter instalado o GIT

5. Não é necessário ter instalado o `ionic` e o `cordova`, pois será utilizado o `npx` para utlizar o ionic e o cordova do `package.json`

6. Ter configurado variáveis de ambiente para `android-studio/gradle/gradle-4.1/bin`, `Android/Sdk/platform-tools`, `Android/Sdk/tools/bin`, `Android/Sdk` e `java-8-openjdk-amd64/bin`.

7. Realizar clone do repositório da aplicação

   1. ```bash
      git clone https://gitlab.com/devcata/app.matriz.git
      cd app.matriz
      ```

8. Realizar instalação das dependências da aplicação

   1. ```bash
      npm i
      ```

9. Adicionar a plataforma android na aplicação

   1. ```bash
      npx ionic cordova platform add android
      ```

10. Para testar a aplicação em um dispositivo android conectado via cabo USB

  1. ```bash
     npx ionic cordova run android -lc
     ```

11. Para gerar o APK da aplicação

   1. ```bash
      npx ionic cordova build android --prod --release
      ```

   2. O APK da aplicação será gerado no caminho `platforms/android/app/build/outputs/apk/release/`

12. Em caso de erro `cordova is not available` no passo `8` , `9` ou na inicialização da aplicação, realizar a seguinte alteração.

    1. Abrir o arquivo `node_modules/@ionic/app-scripts/dist/dev-server/server-config.js`
    2. Substituir a linha `exports.ANDROID_PLATFORM_PATH = path.join('platforms', 'android', 'assets', 'www');` por `exports.ANDROID_PLATFORM_PATH = path.join('platforms', 'android', 'app', 'src', 'main', 'assets', 'www');`
    3. Repetir passo `8` ou `9`

13. Porém caso queira apenas o APK já gerado, baixar em `https://gitlab.com/devcata/app.matriz/tree/master/apk`

#### Configuração do servidor da API

> Esta configuração deve ser realizada antes do uso da aplicação, pois a mesma serve os dados consumidos dentro da aplicação.

1. Realizar clone da API

   * ```bash
     git clone https://gitlab.com/devcata/api.matriz.git
     cd api.matriz
     ```

2. Realizar instalação das dependências da aplicação

   * ```bash
     npm i
     ```

3. Criar a tabela `TBAPPE` e `TBAPPI` no servidor SQL Server

   * ```mssql
     CREATE TABLE TBAPPE(
         id INTEGER PRIMARY KEY IDENTITY NOT NULL,
         dt_ini VARCHAR(10) NOT NULL,
         hr_ini VARCHAR(8) NOT NULL,
         filial VARCHAR(2) NOT NULL,
         recurso VARCHAR(6) NOT NULL,
         produto VARCHAR(15) NULL,
         turno VARCHAR(1) NULL,
         turma VARCHAR(1) NULL,
         quantidade FLOAT NULL,
         resultado VARCHAR(7) NULL,
         medicao_ini FLOAT NULL,
         motivo VARCHAR(6) NULL,
         ccusto VARCHAR(9) NOT NULL,
         operador VARCHAR(6) NULL,
         usuario VARCHAR(6) NOT NULL,
         dt_import VARCHAR(10) NULL
      )

     CREATE TABLE TBAPPI (
     	id INTEGER PRIMARY KEY IDENTITY NOT NULL,
     	filial VARCHAR(2) NOT NULL,
     	dt_ini VARCHAR(10) NOT NULL,
     	dt_final VARCHAR(10) NOT NULL,
     	hr_ini VARCHAR(5) NOT NULL,
     	hr_final VARCHAR(5) NOT NULL,
     	ccusto VARCHAR(9) NOT NULL,
     	motivo VARCHAR(6) NOT NULL,
     	recurso VARCHAR(6) NOT NULL,
     	usuario VARCHAR(6) NOT NULL,
     	dt_import VARCHAR(10) NULL,
     )
     ```

4. Dentro da pasta da API, abrir o arquivo `server.js` e configurar a porta na qual a aplicação API ficará rodando e a conexão com o servidor SQL Server

   * ```javascript
     const port = 3000; // número da porta

     const db = {
         server: '0.0.0.0', // IP do servidor SQL SERVER
         port: 0000, // Porta do SQL SERVER
         database: 'TESTE', // Nome do banco que foram criadas as tabelas no passo 3  
         user: 'usuario', // usuario do banco
         pass: 'senha' // senha do banco
     };
     ```

5. Após configurações, caso queira rodar a API em modo teste é necessário instalar o `nodemon` via `npm install -g nodemon`.

6. Para rodar a API em modo produção, instalar o `PM2` e seguir a documentação de implantação encontrada do site do mesmo.

7. Instalar o APK gerado no dispositivo Android.

8. Entrar com a matricula administrador `101001` e configurar o `IP` de acordo com o IP do servidor SQL Server, `porta` de acordo com a porta cofigurada na API e `filial` para `01`.

9. Salvar as configuração no botão flutuante direito. Após salvar, clicar em OK e baixar os dados para o dispositivo no botão flutuante esquerdo. Após carregamento dos dados, logar com uma matrícula de um funcionário válido da filial `01`.

10. Este processo de configuração dentro da aplicação e carregamento dos dados é feito apenas a primeira vez, caso queira atualizar os dados, basta logar com matricula `101001` e clicar no botão flutuante esquerdo para carregar os dados novamente.no

