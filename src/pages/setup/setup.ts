import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Network } from '@ionic-native/network';

import { DatabaseProvider } from '../../providers/database/database';
import { RequestProvider } from '../../providers/request/request';

@Component({
  selector: 'page-setup',
  templateUrl: 'setup.html',
})
export class SetupPage {

  private setup = { id: null, ip: null, porta: null, matricula: null, filial: null };
  private formGroup: FormGroup;

  constructor
    (
    private nav: NavController,
    private params: NavParams,
    private formBuilder: FormBuilder,
    private storage: DatabaseProvider,
    private alert: AlertController,
    private request: RequestProvider,
    private loading: LoadingController,
    private network: Network
    ) {

    this.formGroup = this.formBuilder.group({
      ip: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(15), Validators.pattern('^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$')]],
      porta: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(5), Validators.pattern('^([0-9]){1,5}')]],
      matricula: ['', [Validators.required, Validators.min(6), Validators.maxLength(6)]],
      filial: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(2)]]
    });

    this.storage.GetSetup().then(res => {
      console.log(JSON.stringify(res));
      this.setup.id = res[0].id;
      this.setup.ip = res[0].ip;
      this.setup.porta = res[0].porta;
      this.setup.matricula = res[0].matricula,
        this.setup.filial = res[0].filial;
    }).catch(error => console.log(error));

  }

  private Salvar(): void {

    let alertSuccess = this.alert.create({
      title: 'Configuração',
      subTitle: 'Configuração atualizada',
      buttons: [
        { text: 'Login', handler: () => { this.nav.pop(); } },
        { text: 'Ok' }
      ]
    });

    let alertFailure = this.alert.create({
      title: 'Configuração',
      subTitle: 'Erro ao atualizar',
      buttons: ['OK']
    });

    this.storage.SaveSetup(this.setup).then(data => {
      console.log(JSON.stringify(data));
      alertSuccess.present();
    }, (error) => {
      console.log(JSON.stringify(error));
      alertFailure.present();
    })
  }

  private Download(): void {
    let alert = this.alert.create({
      title: 'Configuração',
      subTitle: 'Deseja atualizar os dados?',
      buttons: [
        { text: 'Sim', handler: () => { this.UpdateData(); } },
        { text: 'Não' }
      ]
    }).present();
  }

  private async UpdateData() {

    if (this.isDisconnected()) {
      let alert = this.alert.create({
        title: 'Rede',
        subTitle: 'Ops! Verifique sua conexão com a rede.',
        buttons: ['OK']
      }).present();
    } else {

      let loading = this.loading.create({
        content: 'Carregando'
      });

      loading.present();

      let isOK = true;
      await this.DropAndCreateTables();

      await this.request.getResources(this.setup).then(async (data) => {
        for (let i = 0; i < Object.keys(data).length; i++) {
          let objRecurso = {
            filial: data[i]['FILIAL'],
            codigo: data[i]['CODIGO'],
            descricao: data[i]['DESCRICAO'],
            ccusto: data[i]['CCUSTO']
          }
          await this.storage.insertRecursos(objRecurso);
        }
      }, (error) => {
        isOK = false;
        console.log(error);
      });


      if (isOK) {
        await this.request.getCosts(this.setup).then(async (data) => {
          for (let i = 0; i < Object.keys(data).length; i++) {
            let objCusto = {
              ccusto: data[i]['CCUSTO'],
              descricao: data[i]['DESCRICAO']
            }
            await this.storage.insertCustos(objCusto);
          }
        }, (error) => {
          isOK = false;
          console.log(error);
        });
      }

      if (isOK) {
        await this.request.getReasons(this.setup).then(async (data) => {
          for (let i = 0; i < Object.keys(data).length; i++) {
            let objMotivo = {
              filial: data[i]['FILIAL'],
              motivo: data[i]['MOTIVO'],
              descricao: data[i]['DESCRICAO'],
              ccusto: data[i]['CCUSTO']
            }
            await this.storage.insertMotivos(objMotivo);
          }
        }, (error) => {
          isOK = false;
          console.log(error);
        });
      }

      if (isOK) {
        await this.request.getEmployee(this.setup).then(async (data) => {
          for (let i = 0; i < Object.keys(data).length; i++) {
            let objFuncionario = {
              filial: data[i]['FILIAL'],
              matricula: data[i]['MATRICULA'],
              nome: data[i]['NOME']
            }
            await this.storage.insertFuncionarios(objFuncionario);
          }
        }, (error) => {
          isOK = false;
          console.log(error);
        });
      }

      if (isOK) {
        await this.request.getProducts(this.setup).then(async (data) => {
          for (let i = 0; i < Object.keys(data).length; i++) {
            let objProduto = {
              codigo: data[i]['CODIGO'],
              descricao: data[i]['DESCRICAO'],
              ccusto: data[i]['CCUSTO']
            }
            await this.storage.insertProdutos(objProduto);
          }
        }, (error) => {
          isOK = false;
          console.log(error);
        });
      }

      if (isOK) {
        let alert = this.alert.create({
          title: 'Configuração',
          subTitle: 'Dados carregados com sucesso.',
          buttons: [
            { text: 'Login', handler: () => { this.nav.pop(); } },
            { text: 'OK'}
          ]
        }).present().then(() => loading.dismiss());
      } else {
        this.DropAndCreateTables();
        let alert = this.alert.create({
          title: 'Rede',
          subTitle: 'Ops! Falha na conexão com o servidor.',
          buttons: ['OK']
        }).present().then(() => loading.dismiss());
      }

      console.log('UpdateData executado!');
    }
  }

  private async DropAndCreateTables() {
    await this.storage.DropTables();
    await this.storage.CreateTables();
  }

  private isDisconnected() {
    if (this.network.type == 'none')
      return true
    return false;
  }

}






