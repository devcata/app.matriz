import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { SelectSearchable } from 'ionic-select-searchable';
import { DatabaseProvider } from '../../providers/database/database';

import { PointPage } from '../point/point';

@Component({
  selector: 'page-costsresources',
  templateUrl: 'costsresources.html',
})
export class CostsresourcesPage {

  private costs: any;
  private cost: any;

  private isResources = false;

  private dbresources: any;
  private resources: any;
  private data: any;

  constructor
    (
    private nav: NavController,
    private params: NavParams,
    private storage: DatabaseProvider,
    private alert: AlertController
    ) {

    this.storage.getCustosForEficiencia()
      .then(data => {
        this.costs = data;
      });

  }

  ionViewWillEnter() {
    this.data = this.params.data;
  }

  /*
  // Método chamado ao esoclher opção do select combobox
  CostChange(event: { component: SelectSearchable, value: any }) {
    //console.log('Custo:', JSON.stringify(event.value));
    if (event.value) {
      this.storage.getRecursosByCost(event.value)
        .then(data => {
          if (Object.keys(data).length === 0) {
            this.alert.create({
              title: 'Recursos',
              subTitle: `Não foi encontrado recursos para o centro de custo: <br><br>
                (${event.value.ccusto}-${event.value.descricao})`,
              buttons: ['OK']
            }).present();
          } else {
            //console.log('Tem recursos');
          }
          this.dbresources = data;
          this.resources = data;
          this.isResources = true;
        })
    } else {
      this.dbresources = [];
          this.resources = [];
          this.isResources = false;
    }

  }
  */

  CostChange($event) {
    this.PointPage(this.cost);
  }

  // Template do nome do item exibido na view
  CostItemTemplate(cost: any) {
    return `${cost.ccusto}-${cost.descricao}`;
  }

  /*
  private GetitemsDbResources(event: any) {

    let value = event.target.value;
    //console.log(value);

    if (!value || value.trim() == '') {
      this.resources = this.dbresources;
      return;
    }
    this.resources = this.dbresources.filter((v) => {
      if ((v.descricao.toUpperCase().indexOf(value.toUpperCase()) > -1) || (v.codigo.toUpperCase().indexOf(value.toUpperCase()) > -1)) {
        return true;
      }
      return false;
    });

  }
  */


  /*
  private PointPage(objResource) {
    this.data['recurso'] = objResource;
    this.nav.push(PointPage, this.data);
  }
  */

  private PointPage(objCost) {
    if (objCost) {
      this.data['custo'] = objCost;
      //console.log(JSON.stringify(this.data['custo']));
      this.nav.push(PointPage, this.data);
    }
  }

}
