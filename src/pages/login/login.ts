import { Component } from '@angular/core';
import { AlertController, NavController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { MenuPage } from '../menu/menu';
import { SetupPage } from '../setup/setup';

import { DatabaseProvider } from '../../providers/database/database';
import { RequestProvider } from '../../providers/request/request';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  //private employee = { filial: null, matricula: null, nome: null };
  private setup: any; //= { id: null, ip: null, porta: null, matricula: null, filial: null };
  private formGroup: FormGroup;
  //private matriculaAdm = ['1040100'];

  constructor
    (
    private nav: NavController,
    private formBuilder: FormBuilder,
    private storage: DatabaseProvider,
    private request: RequestProvider,
    private alert: AlertController
    ) {

    //Validação dos campos do formulário de login
    this.formGroup = this.formBuilder.group({
      matricula: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]]
    });

    //this.initializeSetup();
  }

  initializeSetup() {
    this.storage.GetSetup().then(data => {
      //this.setup.id = data[0].id;
      //this.setup.ip = data[0].ip;
      //this.setup.porta = data[0].porta;
      //this.setup.matricula = data[0].matricula;
      //this.setup.filial = data[0].filial;
      this.setup = data[0];
      console.log(JSON.stringify(this.setup));
    }).catch(error => console.log(error));
  }

  ionViewDidEnter() {
    //console.log('DidEnter');
    //this.initializeSetup();
  }

  private async Login() {

    this.storage.GetSetup().then(data => {
      this.setup = data[0];
      console.log(JSON.stringify(this.setup));

      this.validarLogin();

    }).catch(error => console.log(error));
  }

  private async validarLogin() {
    
    const matricula = this.formGroup.value.matricula;
    console.log('Matricula Form: ', matricula);
    console.log('Matricula DB: ', this.setup['matricula']);

    if (matricula === this.setup['matricula']) {
      this.SetupPage();
    } else {

      await this.storage.getFuncionario(matricula).then( ( matricula: string ) => {

        if ( matricula ){
          this.MenuPage(this.setup, matricula);
        } else {
          let alert = this.alert.create({
            title: 'Login',
            subTitle: 'Matrícula inválida',
            buttons: [{ text: 'OK', handler: () => this.formGroup.reset() }]
          }).present();
        }

      });
     
      /*
      this.getEmployee(this.setup, matricula).then(data => {
        if (data['matricula'] !== null) {
          //console.log('Matricula válida');
          //console.log(JSON.stringify(data));
          this.MenuPage(this.setup, matricula);
        } else {
          //console.log('Matricula inválida');
          let alert = this.alert.create({
            title: 'Login',
            subTitle: 'Matrícula inválida',
            buttons: [{ text: 'OK', handler: () => this.formGroup.reset() }]
          }).present();
        }
      }).catch(error => {
        let alert = this.alert.create({
          title: 'Login',
          subTitle: 'Falha na conexão com o servidor',
          buttons: [{ text: 'OK', handler: () => this.formGroup.reset() }]
        }).present();
      });
      */
    }
  }

  private SetupPage(): void {
    this.formGroup.reset();
    this.nav.push(SetupPage);
  }

  private MenuPage(setup, mat): void {
    this.formGroup.reset();
    let data: any = {};
    data['setup'] = setup;
    data['usuario'] = { matricula: mat };
    this.nav.setRoot(MenuPage, data);
  }

  private getEmployee(objSetup, matricula) {
    return new Promise((resolve, reject) => {
      this.request.getEmployee(objSetup, matricula)
        .then(data => {
          if (data[0])
            resolve({ filial: data[0].FILIAL, matricula: data[0].MATRICULA, nome: data[0].NOME });
          else
            resolve({ matricula: null });
        }, (error) => reject(error));
    })

  }

}
