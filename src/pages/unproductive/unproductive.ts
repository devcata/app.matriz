import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { Unproductive1Page } from '../unproductive1/unproductive1';
import { Unproductive3Page } from '../unproductive3/unproductive3';
import { DatabaseProvider } from '../../providers/database/database';
import { Network } from '@ionic-native/network';
import { RequestProvider } from '../../providers/request/request';

@Component({
  selector: 'page-unproductive',
  templateUrl: 'unproductive.html',
})
export class UnproductivePage {

  private dbUnproductive: Array<Object>;
  private length: number = 0;
  //private improdutivoSemDataHora: number = 0;
  //private sincronizados: number = 0;
  //private naoSincronizados: number = 0;

  constructor(
    private nav: NavController,
    private params: NavParams,
    private storage: DatabaseProvider,
    private alert: AlertController,
    private network: Network,
    private request: RequestProvider,
    private loading: LoadingController) {

  }

  private ImprodutivoPage1() {
    this.nav.push(Unproductive1Page, this.params.data);
  }

  ionViewWillEnter() {
    console.log(JSON.stringify(this.params.data));
    this.initializeUnproductive();
  }

  private initializeUnproductive(): void {
    this.storage.getCabecImpro().then((data: Array<Object>) => {
      this.dbUnproductive = data;
      this.length = Object.keys(this.dbUnproductive).length;
    })
  }

  private updateCabecImpro(cabecImpro: Object) {
    this.nav.push(Unproductive3Page, cabecImpro)
  }

  private deleteImprodutivo(hash: string) {
    console.log(hash);
    this.alert.create({
      title: 'Improdutivo',
      message: 'Deseja excluir este improdutivo?',
      buttons: [
        {
          text: 'Sim', handler: () => {

            Promise.all([this.storage.deleteCabecImpro(hash), this.storage.deleteCorpImpro(hash)])
              .then(() => {
                this.initializeUnproductive();
              })
              .then(() => {
                this.alert.create({
                  title: 'Improdutivo',
                  message: 'Improdutivo excluído',
                  buttons: ['OK']
                }).present();
              })
          }
        },
        { text: 'Não' }
      ]
    }).present();
  }

  private async sincronizar() {



    if (this.isDisconnected()) {
      let alert = this.alert.create({
        title: 'Rede',
        subTitle: 'Ops! Verifique sua conexão com a rede.',
        buttons: ['OK']
      }).present();
    } else if (this.dbUnproductive.length === 0) {
      this.alert.create({
        title: 'Improdutivo',
        message: 'Não existe improdutivo para sincronizar!',
        buttons: ['OK']
      }).present();
    } else {

      let load = this.loading.create({ content: 'Sincronizando' });
      load.present();

      let improdutivoSemDataHora: number = 0;
      let sincronizados: number = 0;
      let naoSincronizados: number = 0;

      let msgDeImprodutivoSemData = '';
      let msgDeImprodutivoEnviado = '';
      let msgDeImprodutivoNaoEnviado = '';

      for (let i = 0; i < this.dbUnproductive.length; i++) {

        if (!this.dbUnproductive[i]['dt_final'] && !this.dbUnproductive[i]['hr_final']) {
          improdutivoSemDataHora += 1;
          console.log(improdutivoSemDataHora);
        }
        else {
          console.log('1. Entrou no ELSE');
          let recursos: Array<Object>;
          // Pega todos os recursos do improdutivo
          await this.storage.getCorpImpro(this.dbUnproductive[i]['hash'])
            .then((data: Array<Object>) => {
              recursos = data;
              console.log('2. Carregou Recursos do laço: ' + i);
            });

          // Percorre o recurso um por um e para cada um recurso
          // enviar o improdutivo completo para o servidor
          //recursos.forEach(async (value) => {
          //});
          for (let j = 0; j < recursos.length; j++) {
            console.log('3. Entrou no laço do recurso: ' + j);

            let objImpro = { cabecalho: this.dbUnproductive[i], corpo: recursos[j] }

            await this.request.postImprodutivo(this.params.data, objImpro)
              .then(() => {
                this.storage.deleteCabecImpro(this.dbUnproductive[i]['hash'])
                  .then(() => {
                    this.storage.deleteCorpImpro(this.dbUnproductive[i]['hash']);
                  })
              })
              .catch(() => naoSincronizados += 1)
          }
          console.log('4. Saindo do laço do recurso');
          console.log('------------------------------------------------------');
        } // fim else
      } // fim for

      console.log('Total de sincronizados: ', sincronizados);
      console.log('Total não sincronizados: ', naoSincronizados);
      console.log('Total sem data e hora final: ', improdutivoSemDataHora);

      console.log('5. Testando IFS');
      if (improdutivoSemDataHora > 0) {
        console.log(improdutivoSemDataHora + ' sem data e hora final');
        msgDeImprodutivoSemData = `Existe ${improdutivoSemDataHora} ${improdutivoSemDataHora > 1 ? 'improdutivos' : 'improdutivo'} sem data e hora final que ${improdutivoSemDataHora > 1 ? 'não puderam ser sincronizados' : 'não pode ser sincronizado'}. 
          Preecha a data e hora final e depois sincronize`;
        console.log(msgDeImprodutivoSemData);
      }


      if (improdutivoSemDataHora >= 0 && naoSincronizados === 0) {
        sincronizados = this.dbUnproductive.length - improdutivoSemDataHora;
        msgDeImprodutivoEnviado = `${sincronizados} ${sincronizados > 1 ? 'Improdutivos enviados' : 'Improdutivo enviado'}`;
      } else {
        sincronizados = (this.dbUnproductive.length - improdutivoSemDataHora) - naoSincronizados;
        msgDeImprodutivoEnviado = `${sincronizados} ${sincronizados > 1 ? 'Improdutivos enviados' : 'Improdutivo enviado'}`;
      }

      if (naoSincronizados > 0) {
        console.log(naoSincronizados + ' não sincronizados');
        msgDeImprodutivoNaoEnviado = `Um ou mais improdutivos não foi sincronizado devido a falha de conexão`;
        console.log(msgDeImprodutivoNaoEnviado);
      }

      load.dismiss().then(() => {
        console.log('6. Apresentando mensagem');
        this.alert.create({
          title: 'Improdutivo',
          message: `
              ${improdutivoSemDataHora > 0 ? msgDeImprodutivoSemData + ' <hr>' : ''}
              ${sincronizados > 0 ? msgDeImprodutivoEnviado + ' <hr>' : ''}
              ${naoSincronizados > 0 ? msgDeImprodutivoNaoEnviado + ' <hr>' : ''}`,
          buttons: [{ text: 'OK', handler: () => this.initializeUnproductive() }]
        }).present();
      })
    } // fim else
  } // fim sincronizar

  private isDisconnected() {
    if (this.network.type == 'none')
      return true
    return false;
  }

}

