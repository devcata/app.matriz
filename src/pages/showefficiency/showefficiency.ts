import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DatabaseProvider } from '../../providers/database/database';

@Component({
  selector: 'page-showefficiency',
  templateUrl: 'showefficiency.html',
})
export class ShowefficiencyPage {

  private dbResource: any;
  private dbProduct: any;
  private dbreason: any;
  private dbCost: any;
  private dbEmployee: any;

  constructor(private nav: NavController, private params: NavParams, private storage: DatabaseProvider) { }

  ionViewWillEnter() {
    console.log(JSON.stringify(this.params.data));
    this.initializeResource();
    this.initializeProduct();
    this.initializeReason();
    this.initializeCosts();
    this.initializeEmployee();
  }

  initializeResource() {
    this.storage.getRecursos(this.params.data.recurso, this.params.data.custo)
      .then(data => {
        this.dbResource = data;
        console.log(JSON.stringify(this.dbResource));
      })
  }

  initializeProduct() {
    this.storage.getProdutos(this.params.data.produto, this.params.data.custo)
      .then(data => {
        this.dbProduct = data;
        console.log(JSON.stringify(this.dbProduct));
      })
  }

  initializeReason() {
    if (this.params.data.motivo) {
      this.storage.getMotivos(this.params.data.motivo, this.params.data.custo)
        .then(data => {
          this.dbreason = data;
          console.log(JSON.stringify(this.dbreason));
        });
    } else {
      this.dbreason = [];
    }
  }

  initializeCosts() {
    this.storage.getCustos(this.params.data.custo)
      .then(data => {
        this.dbCost = data;
        console.log(JSON.stringify(this.dbCost));
      })
  }

  initializeEmployee() {
    if (this.params.data.operador) {
      this.storage.getFuncionarios(this.params.data.operador)
        .then(data => {
          this.dbEmployee = data;
          console.log(JSON.stringify(this.dbEmployee));
        })
    } else {
      this.dbEmployee = [];
    }
  }

}
