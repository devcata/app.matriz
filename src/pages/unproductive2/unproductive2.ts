import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Md5 } from 'ts-md5/dist/md5';
import moment from 'moment';

import { DatabaseProvider } from '../../providers/database/database';

import { UnproductivePage } from '../unproductive/unproductive';

@Component({
  selector: 'page-unproductive2',
  templateUrl: 'unproductive2.html',
})
export class Unproductive2Page {

  constructor(
    private nav: NavController,
    private params: NavParams,
    private storage: DatabaseProvider,
    private loading: LoadingController,
    private alert: AlertController) {
  }

  ionViewWillEnter() { }

  private async apontar(): Promise<any> {

    let load = this.loading.create({
      content: 'Salvando'
    })
    
    load.present();

    this.salvarImprodutivo()
      .then(() => {
        load.dismiss().then(() => {
          let alert = this.alert.create({
            title: 'Improdutivo',
            subTitle: 'Improdutivo apontado com sucesso',
            buttons: [
              { text: 'Ok', handler: () => { this.nav.popTo(this.nav.getByIndex(this.nav.length()-3)) } },
            ]
          }).present();
        })
      })
      .catch(() => {
        let alert = this.alert.create({
          title: 'Improdutivo',
          subTitle: 'Não foi possível apontar improdutivo',
          buttons: [
            { text: 'Ok' }
          ]
        }).present();
      })
  }

  private salvarImprodutivo() {
    return new Promise((resolve, reject) => {

      let hash = Md5.hashStr(moment().format('DD/MM/YYYY') + moment().format('HH:mm:ss')).toString();

      this.storage.insertCabecImpro(hash, this.params.data).then(() => {

        this.params.data.recurso.forEach(async (data) => {
          await this.storage.insertCorpImpro(hash, data)
        });

        resolve();

      }).catch(() => {
        this.storage.deleteCabecImpro(hash.toString());
        reject();
      })
    })
  }
}
