import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { EfficiencyPage } from '../efficiency/efficiency';
import { UnproductivePage } from '../unproductive/unproductive';

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  private data: any;

  constructor(private nav: NavController, private params: NavParams) {}

  ionViewWillEnter() {
    this.data = this.params.data;
  }

  private EficienciaPage(): void {
    this.nav.push(EfficiencyPage, this.data);
  }

  private ImprodutivoPage(): void {
    this.nav.push(UnproductivePage, this.data);
  }

}
