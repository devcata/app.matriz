import { Component } from '@angular/core';
import { LoadingController, AlertController, NavController, NavParams } from 'ionic-angular';

import { DatabaseProvider } from '../../providers/database/database';
import { RequestProvider } from '../../providers/request/request';

import { CostsresourcesPage } from '../costsresources/costsresources';
import { ShowefficiencyPage } from '../showefficiency/showefficiency';
import { Network } from '@ionic-native/network';

@Component({
  selector: 'page-efficiency',
  templateUrl: 'efficiency.html',
})
export class EfficiencyPage {

  private data: any;
  private arrayEficiencias: any;
  private Length: Number = 0;

  private send: boolean = false;

  constructor(private loading: LoadingController, private request: RequestProvider, private network: Network, private nav: NavController, private params: NavParams, private storage: DatabaseProvider, private alert: AlertController) { }

  initializeEfficiency() {
    this.data = this.params.data;
    //console.log(JSON.stringify(this.data));
    this.storage.getEficiencias()
      .then(data => {
        this.arrayEficiencias = data;
        this.Length = Object.keys(this.arrayEficiencias).length;
      })
  }

  ionViewWillEnter() {

    this.initializeEfficiency();
  }

  private CustoRecursosPage() {
    this.nav.push(CostsresourcesPage, this.data);
  }

  private delete(id) {
    this.storage.deleteEficiencia(id)
      .then((data) => {
        console.log(JSON.stringify(data));
        this.alert.create({
          title: 'Eficiência',
          subTitle: 'Apontamento deletado com sucesso.',
          buttons: [
            { text: 'Ok', handler: () => { this.initializeEfficiency() } }
          ]
        }).present();
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
        this.alert.create({
          title: 'Eficiência',
          subTitle: 'Erro ao deletar apontament :(',
          buttons: ['Ok']
        }).present();
      })
  }

  private ShowEfficiency(objEfficiency) {
    this.nav.push(ShowefficiencyPage, objEfficiency);
  }

  Sincronizar() {
    if (this.Length === 0) {
      this.alert.create({
        title: 'Mensagem',
        subTitle: 'Ops! Não há dados para serem sincronizados.',
        buttons: ['OK']
      }).present();
    } else if (this.isDisconnected()) {
      this.alert.create({
        title: 'Rede',
        subTitle: 'Ops! Verifique sua conexão com a rede.',
        buttons: ['OK']
      }).present();
    } else {
      this.sendEfficiency();
    }
  }

  isDisconnected() {
    if (this.network.type == 'none')
      return true
    return false;
  }

  async sendEfficiency() {

    let loading = this.loading.create({
      content: 'Sincronizando...'
    });

    loading.present();

    for (let i = 0; i < Object.keys(this.arrayEficiencias).length; i++) {
      await this.request.postEficiencia(this.data, this.arrayEficiencias[i])
        .then(async (rs) => {
          await this.storage.deleteEficiencia(this.arrayEficiencias[i].id)
            .then(() => this.send = true)
        })
        .catch(error => {
          this.send = false;
        });

      if (!this.send) {
        loading.dismiss().then(() => {
          this.alert.create({
            title: 'Falha ao sincronizar',
            subTitle: 'Houve uma falha na sincronização dos dados, verifique a conexão com o servidor',
            buttons: [
              { text: 'Ok', handler: () => { this.initializeEfficiency() } }
            ]
          }).present();
        })
        break;
      }
    }

    if (this.send) {
      loading.dismiss().then(() => {
        this.alert.create({
          title: 'Eficiência',
          subTitle: 'Dados sincronizados com sucesso',
          buttons: [
            { text: 'Ok', handler: () => { this.initializeEfficiency() } }
          ]
        }).present();
      });
    }
  }

}
