import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DatabaseProvider } from '../../providers/database/database';

@Component({
  selector: 'page-unproductive3',
  templateUrl: 'unproductive3.html',
})
export class Unproductive3Page {

  private dbCorpImpro: Array<Object>;
  private formGroup: FormGroup;

  constructor(
    private nav: NavController, 
    private params: NavParams,
    private storage: DatabaseProvider,
    private formBuilder: FormBuilder,
    private alert: AlertController) {

      this.formGroup = this.formBuilder.group({
        dt_ini: [''],
        dt_final: ['', [Validators.required]],
        hr_ini: [''],
        hr_final: ['', [Validators.required]],
      });
  }

  ionViewWillEnter() {
    console.log(JSON.stringify(this.params.data));
    this.initializeCorpImpro();
  }

  initializeCorpImpro(): void {
    this.storage.getCorpImpro(this.params.data.hash)
      .then((data: Array<Object>) => {
        this.dbCorpImpro = data;
        console.log(JSON.stringify(this.dbCorpImpro));
      })
  }

  private update(){
    this.storage.updateCabecImpro(this.params.data.hash, this.params.data.dt_final, this.params.data.hr_final)
      .then(() => {
        this.alert.create({
          title: 'Improdutivo',
          message: 'Improdutivo atualizado',
          buttons: [
            { text: 'OK', handler: () => { this.nav.pop() }}
          ]
        }).present();
      })
  }

}
