import { Component, ViewChild } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';

import { SelectSearchable } from 'ionic-select-searchable';

import { DatabaseProvider } from '../../providers/database/database';
import moment from 'moment';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'page-point',
  templateUrl: 'point.html',
})
export class PointPage {

  private formGroup: FormGroup;

  private dbProducts: any;
  private product: any;

  private dbResources: any;
  private resource: any;

  private dbEmployees: any;
  private employee: any;
  private isEmployee: boolean = false;


  private dbReasons: any;
  private reason: any;
  private isMotivo: boolean = false;

  private data: any;

  private turno: any;
  private turma: any;

  private quantidade: any;
  private resultado: any;
  private dt_ini: any;
  private hr_ini: any;

  private codigo_p: string;
  private flagCache: boolean = false;

  @ViewChild('inputToFocus') inputToFocus;

  constructor
    (
    private nav: NavController,
    private params: NavParams,
    private storage: DatabaseProvider,
    private alert: AlertController,
    private formBuilder: FormBuilder,
  ) {

    this.formGroup = this.formBuilder.group({
      dt_ini: ['', [Validators.required]],
      hr_ini: ['', [Validators.required]],
      product: ['', [Validators.required]],
      resource: ['', [Validators.required]],
      turno: ['', [Validators.required]],
      turma: ['', [Validators.required]],
      quantidade: ['', [Validators.required]],
      resultado: ['', [Validators.required]],
      employee: [''],
      reason: ['']
    });


  }

  ionViewWillEnter() {

    this.initializeDate();

    this.data = this.params.data;

    console.log(JSON.stringify(this.data));
    console.log(this.data['custo'].ccusto);
    this.initializeProducts();
    this.initializeResources();

  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.inputToFocus.setFocus();
    }, 200)
  }

  async ResourceChange($event) {

    await this.storage.selectCache(this.resource.codigo, this.data['custo'].ccusto)
      .then((res: Array<string>) => {
        //console.log( res );
        if (res.length > 0) {
          this.flagCache = true;
          //console.log('existe um último apontamento de produto para este recurso');
          this.codigo_p = res[0];
          this.turno = res[1];
          this.turma = res[2];
          this.resultado = res[3];
          //console.log(this.codigo_p);
          for (let i = 0; i < this.dbProducts.length; i++) {
            if (this.dbProducts[i]['codigo'] === this.codigo_p) {
              //console.log(JSON.stringify(this.dbProducts[i]));
              this.product = this.dbProducts[i];
              break;
            }
          }
        } else { this.flagCache = false; }
      });
  }

  private initializeProducts() {
    this.storage.getProdutosByCost(this.data['custo'].ccusto)
      .then(data => {
        this.dbProducts = data;
        console.log(JSON.stringify(this.dbProducts));
      })
  }

  private initializeResources() {
    this.storage.getRecursosByCost(this.data['custo'].ccusto)
      .then(data => {
        this.dbResources = data;
        console.log(JSON.stringify(this.dbResources));
      })
  }

  private initializeReasons() {
    this.storage.getMotivosByCost(this.data['custo'].ccusto)
      .then(data => {
        this.dbReasons = data;
        console.log(JSON.stringify(this.dbReasons));
      })
  }

  private initializeEmployees() {
    this.storage.getFuncionarios()
      .then(data => {
        this.dbEmployees = data;
        console.log(JSON.stringify(this.dbEmployees));
      })
  }

  private initializeDate() {
    this.dt_ini = moment().locale('pt-br').format('YYYY-MM-DD');
    this.hr_ini = moment().locale('pt-br').format('HH:mm:ss');
  }

  // Template do nome do item exibido na view
  ProductItemTemplate(product: any) {
    return `${product.descricao}`;
  }

  ResourceItemTemplate(resource: any) {
    return `${resource.codigo}-${resource.descricao}`;
  }

  ReasonItemTemplate(reason: any) {
    return `${reason.motivo}-${reason.descricao}`;
  }
  EmployeeItemTemplate(employee: any) {
    return `${employee.nome}`;
  }

  private ShowReason(event: any) {

    if (event.checked) {
      this.isMotivo = true;
      this.initializeReasons();
    }
    else {
      this.isMotivo = false;
      this.dbReasons = [];
      this.reason = null;
    }
  }

  private ShowEmployee(event: any) {

    if (event.checked) {
      this.isEmployee = true;
      this.initializeEmployees();
    }
    else {
      this.isEmployee = false;
      this.dbEmployees = [];
      this.employee = null;
    }
  }

  private async SalvarApontamento() {

    this.data['calendario'] = { dt_ini: this.dt_ini, hr_ini: this.hr_ini };
    this.data['produto'] = this.product;
    this.data['recurso'] = this.resource;
    this.data['turno'] = { value: this.turno };
    this.data['turma'] = { value: this.turma };
    this.data['quantidade'] = { value: this.quantidade };
    this.data['resultado'] = { value: this.resultado };
    //this.data['motivo'] = this.reason || null;
    //this.data['operador'] = { matricula: this.employee.matricula || null };

    if (this.isMotivo) {
      if (this.reason) {
        this.data['motivo'] = this.reason || undefined;
      } else {
        this.data['motivo'] = { motivo: null };
      }
    } else {
      this.data['motivo'] = { motivo: null };
    }


    if (this.isEmployee)
      if (this.employee) {
        this.data['operador'] = { matricula: this.employee.matricula };
      } else {
        this.data['operador'] = { matricula: null };
      }
    else
      this.data['operador'] = { matricula: null };


    if (this.flagCache === true) {
      await this.storage.updateCache(this.resource.codigo, this.product.codigo, this.data['custo'].ccusto, this.turno, this.turma, this.resultado);
      this.flagCache = false;
    } else {
      await this.storage.insertCache(this.resource.codigo, this.product.codigo, this.data['custo'].ccusto, this.turno, this.turma, this.resultado);
    }

    await this.storage.insertEficiencia(this.data).then(() => {
      let alertSuccess = this.alert.create({
        title: 'Eficiência',
        subTitle: 'Apontamento realizado com sucesso',
        buttons: [
          { text: 'OK', handler: () => { this.apontarNovamente(); } }
        ]
      }).present();
    });
  }

  private apontarNovamente() {

    this.initializeDate();
    this.quantidade = '';
    //this.resource = null;
    //this.employee = null
    //this.isEmployee = false;
    //this.turma = null;
    this.reason = null;
    //this.resultado = null;
    //this.isMotivo = false;
    this.inputToFocus.setFocus();
  }

}
