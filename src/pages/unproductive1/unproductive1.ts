import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { SelectSearchable } from 'ionic-select-searchable';
import moment from 'moment';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { DatabaseProvider } from '../../providers/database/database';

import { Unproductive2Page } from '../unproductive2/unproductive2';

@Component({
  selector: 'page-unproductive1',
  templateUrl: 'unproductive1.html',
})
export class Unproductive1Page {

  private formGroup: FormGroup;
  private dt_ini: string;
  private dt_final: string;
  private hr_ini: string;
  private hr_final: string;
  private custo: Object;
  private motivo: Object;
  private recurso: Object;

  private dbCosts: Array<Object>;
  private dbReasons: Array<Object>;
  private dbResources: Array<Object>;

  private data: Object;

  constructor(private nav: NavController, private params: NavParams, private formBuilder: FormBuilder, private storage: DatabaseProvider) {

    this.formGroup = this.formBuilder.group({
      dt_ini: ['', [Validators.required]],
      hr_ini: ['', [Validators.required]],
      dt_final: ['', []],
      hr_final: ['', []],
      custo: ['', [Validators.required]],
      motivo: ['', [Validators.required]],
      recurso: ['', [Validators.required]],
    });
  }

  ionViewWillEnter() {
    this.data = this.params.data;
    this.initializeDate();
    this.initializeCosts();
  }

  Relatorio() {
    this.data['calendario'] = {
      dt_ini: this.dt_ini, 
      dt_final: this.dt_final || '', 
      hr_ini: this.hr_ini, 
      hr_final: this.hr_final || ''
    }
    this.data['ccusto'] = this.custo;
    this.data['motivo'] = this.motivo;
    this.data['recurso'] = this.recurso;
    this.nav.push(Unproductive2Page, this.data);
  }

  // Método chamado ao esoclher opção do select combobox
  CostChange(event: { component: SelectSearchable, value: any }) {
    //console.log('Custo:', JSON.stringify(event.value));
    if (event.value) {
      this.motivo = null;
      this.dbReasons = [];
      this.recurso = null;
      this.dbResources = [];
     this.initializeReasons(event.value);
    } else {
      this.motivo = null;
      this.dbReasons = [];
      this.recurso = null;
      this.dbResources = [];
    }
  }

  ReasonChange(event: { component: SelectSearchable, value: any }) {
    if (event.value) {
      this.initializeResources(event.value);
     } else {
       this.recurso = null;
       this.dbResources = [];
     }
  }

  private initializeCosts() {
    this.storage.getCustos()
      .then((rs: Array<Object>) => this.dbCosts = rs)
  }

  private initializeReasons(object: Object) {
    this.storage.getMotivosByCost(object['ccusto'])
      .then((rs: Array<Object>) => {
        this.dbReasons = [];
        this.dbReasons = rs;
      })    
  }

  private initializeResources(object: Object) {
    this.storage.getRecursosByCost(object['ccusto'])
      .then((rs: Array<Object>) => {
        this.dbResources = [];
        this.dbResources = rs;
      })
  }

  // Template do nome do item exibido na view
  private CostItemTemplate(cost: any): string {
    return `${cost.ccusto}-${cost.descricao}`;
  }

  private ReasonItemTemplate(reason: any): string {
    return `${reason.motivo}-${reason.descricao}`;
  }

  private ResourceItemTemplate(resource: any): string {
    return `${resource.codigo}-${resource.descricao}`;
  }

  private initializeDate() {
    this.dt_ini = moment().locale('pt-br').format('YYYY-MM-DD');
    this.hr_ini = moment().locale('pt-br').format('HH:mm');
  }

}
