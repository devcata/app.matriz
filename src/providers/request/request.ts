import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class RequestProvider {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private http: Http) {}

  public getEmployee(objSetup, mat?) {

    let url = `http://${objSetup.ip}:${objSetup.porta}/funcionarios`;

    let Request = {
      filial: null,
      matricula: null
    };

    return new Promise((resolve, reject) => {

      if (mat) {
        Request.filial = objSetup.filial;
        Request.matricula = mat;
      } else {
        Request.filial = objSetup.filial;
        Request.matricula = null;
      }

      this.http.post(url, JSON.stringify(Request), this.options).toPromise().then(data => {
        resolve(data.json());
      }, (error) => reject(error));
    });
  }

  public getResources(objSetup) {

    let url = `http://${objSetup.ip}:${objSetup.porta}/recursos`;

    return new Promise((resolve, reject) => {
      this.http.post(url, JSON.stringify({filial: objSetup.filial}), this.options).toPromise().then(data => {
        resolve(data.json());
      }, (error) => reject(error));
    });
  }

  public getReasons(objSetup) {

    console.log(JSON.stringify(objSetup));

    let url = `http://${objSetup.ip}:${objSetup.porta}/motivos`;

    return new Promise((resolve, reject) => {
      this.http.post(url, JSON.stringify({filial: objSetup.filial}), this.options).toPromise().then(data => {
        resolve(data.json());
      }, (error) => reject(error));
    });
  }

  public getProducts(objSetup) {

    let url = `http://${objSetup.ip}:${objSetup.porta}/produtos`;

    return new Promise((resolve, reject) => {
      this.http.get(url, this.options).toPromise().then(data => {
        resolve(data.json());
      }, (error) => reject(error));
    });    
  }

  public getCosts(objSetup) {

    let url = `http://${objSetup.ip}:${objSetup.porta}/custos`;

    return new Promise((resolve, reject) => {
      this.http.get(url, this.options).toPromise().then(data => {
        resolve(data.json());
      }, (error) => reject(error));
    });    
  }

  public postEficiencia(objSetup, objEficiencia) {

    let url = `http://${objSetup.setup.ip}:${objSetup.setup.porta}/eficiencia`;

    return new Promise((resolve, reject) => {
      this.http.post(url, JSON.stringify(objEficiencia), this.options).toPromise().then(data => {
        resolve(data);
      }, (error) => reject(error));
    });
  }

  public postImprodutivo(objSetup, objImprodutivo) {

    let url = `http://${objSetup.setup.ip}:${objSetup.setup.porta}/improdutivo`;
  
    return new Promise((resolve, reject) => {
      this.http.post(url, JSON.stringify(objImprodutivo), this.options).toPromise().then(data => {
        resolve(true);
      }, (error) => reject(error));
    });
  }
}