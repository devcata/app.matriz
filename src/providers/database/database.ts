import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class DatabaseProvider {

  private db: SQLiteObject;
  private isOpen: boolean;

  constructor(private storage: SQLite) { }

  //============ DDL ==================
  public Create() {
    return new Promise((resolve, reject) => {
      if (!this.isOpen) {
        this.storage = new SQLite();
        this.storage.create({ name: 'cata.db', location: 'default' }).then((db: SQLiteObject) => {
          this.db = db;
          this.isOpen = true;
          resolve({ message: 'db created' });
        }).catch((error) => {
          reject({ erro: error });
        });
      }
    });
  }

  public CreateTables() {
    this.db.sqlBatch([
      ['CREATE TABLE IF NOT EXISTS setup (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ip VARCHAR(15) NOT NULL, porta NUMBER NOT NULL, matricula VARCHAR(6) NOT NULL, filial VARCHAR(2) NOT NULL)'],
      ['CREATE TABLE IF NOT EXISTS recursos (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, filial VARCHAR(2) NOT NULL, codigo VARCHAR(6) NOT NULL, descricao VARCHAR(30) NOT NULL, ccusto VARCHAR(9) NOT NULL)'],
      ['CREATE TABLE IF NOT EXISTS custos (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, ccusto VARCHAR(9) NOT NULL, descricao VARCHAR(40) NOT NULL)'],
      ['CREATE TABLE IF NOT EXISTS motivos (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, filial VARCHAR(2) NOT NULL, motivo VARCHAR(6) NOT NULL, descricao VARCHAR(55) NOT NULL, ccusto VARCHAR(9))'],
      ['CREATE TABLE IF NOT EXISTS funcionarios (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, filial VARCHAR(2) NOT NULL, matricula VARCHAR(6) NOT NULL, nome VARCHAR(30) NOT NULL)'],
      ['CREATE TABLE IF NOT EXISTS produtos (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, codigo VARCHAR(15) NOT NULL, descricao VARCHAR(170) NOT NULL, ccusto VARCHAR(9) NOT NULL)'],
      ['CREATE TABLE IF NOT EXISTS eficiencias (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, dt_ini VARCHAR(10) NOT NULL, hr_ini VARCHAR(5)	NOT NULL, filial VARCHAR(2) NOT NULL, recurso VARCHAR(6) NOT NULL, produto VARCHAR(15) NOT NULL, turno VARCHAR(1) NOT NULL, turma VARCHAR(1) NOT NULL, quantidade REAL NOT NULL, resultado VARCHAR(7) NOT NULL, motivo VARCHAR(6) NULL, ccusto VARCHAR(9) NOT NULL, operador VARCHAR(6) NULL, usuario VARCHAR(6) NOT NULL)'],
      ['CREATE TABLE IF NOT EXISTS cabecImpro (hash VARCHAR NOT NULL, filial VARCHAR(2) NOt NULL, dt_ini VARCHAR(10) NOT NULL, dt_final VARCHAR(10) NULL, hr_ini VARCHAR(5) NOT NULL, hr_final VARCHAR(5) NULL, ccusto VARCHAR(9) NOT NULL, desccusto VARCHAR(40) NOT NULL, motivo VARCHAR(6) NOT NULL, descmotivo VARCHAR(55) NOT NULL, usuario VARCHAR(6) NOT NULL)'],
      ['CREATE TABLE IF NOT EXISTS corpImpro (hash VARCHAR NOT NULL, codigo VARCHAR(6) NOT NULL, descricao VARCHAR(30) NOT NULL)'],
      ['CREATE TABLE IF NOT EXISTS cache (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, codigo_r VARCHAR(6) NULL, codigo_p VARCHAR(15) NULL, ccusto VARCHAR(9) NULL, turno VARCHAR(1) NULL, turma VARCHAR(1) NULL, resultado VARCHAR(7) NULL)']
    ]).then(() => {
      console.log('Tables created successfully')
    }).catch((error) => {
      console.log(JSON.stringify({ message: 'Error creating tables', erro: error }));
    });
  }

  public DropTables() {
    this.db.sqlBatch([
      ['DROP TABLE IF EXISTS recursos'],
      ['DROP TABLE IF EXISTS custos'],
      ['DROP TABLE IF EXISTS motivos'],
      ['DROP TABLE IF EXISTS funcionarios'],
      ['DROP TABLE IF EXISTS produtos']
    ]).then(() => {
      console.log('Tables deleted successfully')
    }).catch((error) => {
      console.log(JSON.stringify({ message: 'Error deleted tables', erro: error }));
    });
  }
  //===================================

  // Manipulação de banco referente a configuração de ip, porta e filial
  //============ DML SETUP ==================
  public SetupDefault() {

    let sql = 'SELECT COUNT(id) as id FROM setup';
    this.db.executeSql(sql, []).then((data) => {
      if (data.rows.item(0).id == 0) {
        this.db.executeSql('INSERT INTO setup (ip, porta, matricula, filial) VALUES(?, ?, ?, ?)', ['0.0.0.0', '000000', '101001', '00'])
          .then(() => {
            console.log(JSON.stringify({ message: 'Setup padrão configurado' }));
          })
          .catch(error => {
            console.log(JSON.stringify({ message: 'Erro na configuração de setup padrão', erro: error }));
          })
      }
    }).catch(error => {
      console.log(JSON.stringify({ message: 'Erro ao consultar setup', erro: error }));
    });
  }

  public GetSetup() {
    return new Promise((resolve, reject) => {
      this.db.executeSql('SELECT id, ip, porta, matricula, filial FROM setup', []).then(data => {
        let arraySetup = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arraySetup.push({
              id: data.rows.item(i).id,
              ip: data.rows.item(i).ip,
              porta: data.rows.item(i).porta,
              matricula: data.rows.item(i).matricula,
              filial: data.rows.item(i).filial
            });
            resolve(arraySetup);
          }
        }

      }, (error) => {
        reject(error);
      })
    })
  }

  public SaveSetup(objSetup) {
    return new Promise((resolve, reject) => {
      const params = [objSetup['ip'], objSetup['porta'], objSetup['matricula'], objSetup['filial'], objSetup['id']];
      this.db.executeSql('UPDATE setup SET ip = ?, porta = ?, matricula = ?, filial = ? WHERE id = ?', params).then(() => {
        resolve({ message: 'Setup atualizado com sucesso' });
      }, (error) => {
        reject({ message: 'Erro ao atualizar setup', erro: error });
      })
    });
  }
  //=========================================


  // Manipulação de banco referente a recursos
  //============ DML recursos ==================
  public insertRecursos(objRecursos) {
    return new Promise((resolve, reject) => {
      let sql = `INSERT INTO recursos (filial, codigo, descricao, ccusto) VALUES (?, ?, ?, ?)`;
      let params = [objRecursos.filial, objRecursos.codigo, objRecursos.descricao, objRecursos.ccusto];
      this.db.executeSql(sql, params).then(() => {
        console.log('Recurso inserido com sucesso!');
        resolve();
      }, (error) => reject(error));
    });
  }

  public getRecursos(value?, custo?) {
    return new Promise((resolve, reject) => {

      let sql = `SELECT id, filial, codigo, descricao, ccusto FROM recursos`;
      let params = [];
      if (value && custo) {
        sql += ` WHERE (codigo LIKE ? OR descricao LIKE ?) AND ccusto = ?`;
        params = [value, value, custo];
      }
      this.db.executeSql(sql, params).then(data => {
        let arrayRecursos = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arrayRecursos.push({
              id: data.rows.item(i).id,
              filial: data.rows.item(i).filial,
              codigo: data.rows.item(i).codigo,
              descricao: data.rows.item(i).descricao,
              ccusto: data.rows.item(i).ccusto
            });
          }
        }
        resolve(arrayRecursos);
      }, (error) => reject(error));
    })
  }

  public getRecursosByCost(ccusto) {
    return new Promise((resolve, reject) => {

      let sql = `SELECT id, filial, codigo, descricao, ccusto, codigo || '-' || descricao AS pesquisa FROM recursos WHERE ccusto = ?`;
      let params = [ccusto];
      //console.log(`getRecursosByCost(${ccusto})`);
      this.db.executeSql(sql, params).then(data => {
        let arrayRecursos = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arrayRecursos.push({
              id: data.rows.item(i).id,
              filial: data.rows.item(i).filial,
              codigo: data.rows.item(i).codigo,
              descricao: data.rows.item(i).descricao,
              ccusto: data.rows.item(i).ccusto,
              pesquisa: data.rows.item(i).pesquisa
            });
          }
        }
        resolve(arrayRecursos);
      }, (error) => reject(error));
    })
  }
  //============================================



  // Manipulação de banco referente a custos
  //============ DML custos ==================
  public insertCustos(objCusto) {
    return new Promise((resolve, reject) => {
      let sql = `INSERT INTO custos (ccusto, descricao) VALUES (?, ?)`;
      let params = [objCusto.ccusto, objCusto.descricao];
      this.db.executeSql(sql, params).then(() => {
        console.log('Custo inserido com sucesso!');
        resolve();
      }, (error) => reject(error));
    });
  }

  public getCustos(value?) {
    return new Promise((resolve, reject) => {

      let sql = `SELECT id, ccusto, descricao, ccusto || '-' || descricao AS pesquisa FROM custos`;
      let params = [];
      if (value) {
        sql += ` WHERE ccusto LIKE ? OR descricao LIKE ?`;
        params = [value, value];
      }
      this.db.executeSql(sql, params).then(data => {
        let arrayCustos = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arrayCustos.push({
              id: data.rows.item(i).id,
              ccusto: data.rows.item(i).ccusto,
              descricao: data.rows.item(i).descricao,
              pesquisa: data.rows.item(i).pesquisa
            });
          }
        }
        resolve(arrayCustos);
      }, (error) => reject(error));
    })
  }

  public getCustosForEficiencia(value?) {
    return new Promise((resolve, reject) => {

      let sql = `SELECT id, ccusto, descricao, ccusto || '-' || descricao AS pesquisa FROM custos WHERE ccusto IN ('3201', '3301', '3401')`;
      let params = [];
      if (value) {
        sql += ` WHERE ccusto LIKE ? OR descricao LIKE ?`;
        params = [value, value];
      }
      this.db.executeSql(sql, params).then(data => {
        let arrayCustos = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arrayCustos.push({
              id: data.rows.item(i).id,
              ccusto: data.rows.item(i).ccusto,
              descricao: data.rows.item(i).descricao,
              pesquisa: data.rows.item(i).pesquisa
            });
          }
        }
        resolve(arrayCustos);
      }, (error) => reject(error));
    })
  }
  //============================================



  // Manipulação de banco referente a motivos
  //============ DML motivos ==================
  public insertMotivos(objMotivo) {
    return new Promise((resolve, reject) => {
      let sql = `INSERT INTO motivos (filial, motivo, descricao, ccusto) VALUES (?, ?, ?, ?)`;
      let params = [objMotivo.filial, objMotivo.motivo, objMotivo.descricao, objMotivo.ccusto];
      this.db.executeSql(sql, params).then(() => {
        console.log('Motivo inserido com sucesso!');
        resolve();
      }, (error) => reject(error));
    });
  }

  public getMotivos(value?, custo?) {
    return new Promise((resolve, reject) => {

      let sql = `SELECT id, filial, motivo, descricao, ccusto FROM motivos`;
      let params = [];
      if (value && custo) {
        sql += ` WHERE (motivo LIKE ? OR descricao LIKE ?) AND ccusto = ?`;
        params = [value, value, custo];
      }
      this.db.executeSql(sql, params).then(data => {
        let arrayMotivos = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arrayMotivos.push({
              id: data.rows.item(i).id,
              motivo: data.rows.item(i).motivo,
              descricao: data.rows.item(i).descricao,
              ccusto: data.rows.item(i).ccusto
            });
          }
        }
        resolve(arrayMotivos);
      }, (error) => reject(error));
    })
  }

  public getMotivosByCost(ccusto) {
    return new Promise((resolve, reject) => {

      let sql = `SELECT id, filial, motivo, descricao, ccusto, motivo || '-' || descricao AS pesquisa FROM motivos WHERE ccusto = ?`;
      let params = [ccusto];

      this.db.executeSql(sql, params).then(data => {
        let arrayMotivos = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arrayMotivos.push({
              id: data.rows.item(i).id,
              motivo: data.rows.item(i).motivo,
              descricao: data.rows.item(i).descricao,
              ccusto: data.rows.item(i).ccusto,
              pesquisa: data.rows.item(i).pesquisa
            });
          }
        }
        resolve(arrayMotivos);
      }, (error) => reject(error));
    })
  }
  //============================================


  // Manipulação de banco referente a funcioanrios
  //============ DML motivos ==================
  public insertFuncionarios(objFuncionario) {
    return new Promise((resolve, reject) => {
      let sql = `INSERT INTO funcionarios (filial, matricula, nome) VALUES (?, ?, ?)`;
      let params = [objFuncionario.filial, objFuncionario.matricula, objFuncionario.nome];
      this.db.executeSql(sql, params).then(() => {
        console.log('Funcionario inserido com sucesso!');
        resolve();
      }, (error) => reject(error));
    });
  }

  public getFuncionarios(value?) {
    return new Promise((resolve, reject) => {

      let sql = `SELECT id, filial, matricula, nome, matricula || '-' || nome AS pesquisa FROM funcionarios`;
      let params = [];

      if (value) {
        sql += ` WHERE matricula = ?`;
        params = [value];
      }

      this.db.executeSql(sql, params).then(data => {
        let arrayFuncionarios = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arrayFuncionarios.push({
              id: data.rows.item(i).id,
              filial: data.rows.item(i).filial,
              matricula: data.rows.item(i).matricula,
              nome: data.rows.item(i).nome,
              pesquisa: data.rows.item(i).pesquisa
            });
          }
        }
        resolve(arrayFuncionarios);
      }, (error) => reject(error));
    })
  }

  public getFuncionario(matricula) {
    return new Promise((resolve, reject) => {

      let sql = `SELECT matricula FROM funcionarios WHERE matricula = ?`;
      let params = [matricula];

      this.db.executeSql(sql, params).then(data => {
        let matricula: string;
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
              matricula = data.rows.item(i).matricula
          }
        }
        resolve(matricula);
      }, (error) => reject(error));
    })
  }


  // Manipulação de banco referente a produtos
  //============ DML produtos ==================
  public insertProdutos(objProduto) {
    return new Promise((resolve, reject) => {
      let sql = `INSERT INTO produtos (codigo, descricao, ccusto) VALUES (?, ?, ?)`;
      let params = [objProduto.codigo, objProduto.descricao, objProduto.ccusto];
      this.db.executeSql(sql, params).then(() => {
        console.log('Produto inserido com sucesso!');
        resolve();
      }, (error) => reject(error));
    });
  }

  public getProdutos(value?, custo?) {
    return new Promise((resolve, reject) => {

      let sql = `SELECT id, codigo, descricao, ccusto FROM produtos`;
      let params = [];
      if (value && custo) {
        sql += ` WHERE (codigo LIKE ? OR descricao LIKE ?) AND ccusto = ?`;
        params = [value, value, custo];
      }
      this.db.executeSql(sql, params).then(data => {
        let arrayProdutos = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arrayProdutos.push({
              id: data.rows.item(i).id,
              codigo: data.rows.item(i).codigo,
              descricao: data.rows.item(i).descricao,
              ccusto: data.rows.item(i).ccusto
            });
          }
        }
        resolve(arrayProdutos);
      }, (error) => reject(error));
    })
  }

  public getProdutosByCost(ccusto) {
    return new Promise((resolve, reject) => {

      let sql = `SELECT id, codigo, descricao, ccusto, codigo || '-' || descricao AS pesquisa FROM produtos WHERE ccusto = ?`;
      let params = [ccusto];
      //console.log(`getProdutosByCost(${ccusto})`);
      this.db.executeSql(sql, params).then(data => {
        let arrayProdutos = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arrayProdutos.push({
              id: data.rows.item(i).id,
              codigo: data.rows.item(i).codigo,
              descricao: data.rows.item(i).descricao,
              ccusto: data.rows.item(i).ccusto,
              pesquisa: data.rows.item(i).pesquisa
            });
          }
        }
        resolve(arrayProdutos);
      }, (error) => reject(error));
    })
  }
  //============================================

  // Manipulação de banco referente a eficiencias
  //============ DML recursos ==================
  public insertEficiencia(objApontamento) {
    return new Promise((resolve, reject) => {
      let sql = `INSERT INTO eficiencias (dt_ini, hr_ini, filial, recurso, produto, turno, turma, quantidade, resultado, motivo, ccusto, operador, usuario) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)`;
      let params = [
        objApontamento['calendario'].dt_ini,
        objApontamento['calendario'].hr_ini,
        objApontamento['setup'].filial,
        objApontamento['recurso'].codigo,
        objApontamento['produto'].codigo,
        objApontamento['turno'].value,
        objApontamento['turma'].value,
        parseFloat(objApontamento['quantidade'].value),
        objApontamento['resultado'].value,
        objApontamento['motivo'].motivo,
        objApontamento['recurso'].ccusto,
        objApontamento['operador'].matricula,
        objApontamento['usuario'].matricula
      ];
      this.db.executeSql(sql, params).then(() => {
        console.log('Eficiencia inserido com sucesso!');
        resolve();
      }, (error) => reject(error));
    });
  }

  public getEficiencias() {
    return new Promise((resolve, reject) => {

      let sql = `SELECT * FROM eficiencias`;

      this.db.executeSql(sql, []).then(data => {
        let arrayEficiencias = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arrayEficiencias.push({
              id: data.rows.item(i).id,
              dt_ini: data.rows.item(i).dt_ini,
              hr_ini: data.rows.item(i).hr_ini,
              filial: data.rows.item(i).filial,
              recurso: data.rows.item(i).recurso,
              produto: data.rows.item(i).produto,
              turno: data.rows.item(i).turno,
              turma: data.rows.item(i).turma,
              quantidade: data.rows.item(i).quantidade,
              resultado: data.rows.item(i).resultado,
              motivo: data.rows.item(i).motivo,
              custo: data.rows.item(i).ccusto,
              operador: data.rows.item(i).operador,
              usuario: data.rows.item(i).usuario
            });
          }
        }
        resolve(arrayEficiencias);
      }, (error) => reject(error));
    })
  }

  public deleteEficiencia(id) {
    return new Promise((resolve, reject) => {
      this.db.executeSql('DELETE FROM eficiencias WHERE id = ?', [id])
        .then((result) => resolve(result))
        .catch((error) => reject(error));
    })
  }

  // Manipulação de banco referente a improdutivos
  //============ DML recursos ==================
  public insertCabecImpro(hash: string, data: Object) {

    return new Promise((resolve, reject) => {
      let sql = `INSERT INTO cabecImpro VALUES (?,?,?,?,?,?,?,?,?,?,?)`;
      let params = [
        hash,
        data['setup'].filial,
        data['calendario'].dt_ini,
        data['calendario'].dt_final || null,
        data['calendario'].hr_ini,
        data['calendario'].hr_final || null,
        data['ccusto'].ccusto,
        data['ccusto'].descricao,
        data['motivo'].motivo,
        data['motivo'].descricao,
        data['usuario'].matricula
      ];

      this.db.executeSql(sql, params).then(() => {
        console.log('cabeçalho do improdutivo inserido com sucesso!');
        resolve();
      }, (error) => reject(error));
    });
  }

  public updateCabecImpro(hash: string, dt_final: string, hr_final: string) {
    return new Promise((resolve, reject) => {
      let sql = `UPDATE cabecImpro SET dt_final = ?, hr_final = ? WHERE hash = ?`;
      let params = [dt_final, hr_final, hash];
      this.db.executeSql(sql, params).then(() => {
        console.log('cabeçalho do improdutivo atualizado com sucesso!');
        resolve();
      }, (error) => reject(error));
    });
  }

  public insertCorpImpro(hash: string, data: Object) {

    return new Promise((resolve, reject) => {
      let sql = `INSERT INTO corpImpro VALUES (?,?,?)`;
      let params = [
        hash,
        data['codigo'],
        data['descricao']
      ];

      this.db.executeSql(sql, params).then(() => {
        console.log('corpo do improdutivo inserido com sucesso!');
        resolve();
      }, (error) => reject(error));
    });
  }

  public deleteCabecImpro(hash: string) {
    return new Promise((resolve, reject) => {
      let sql = `DELETE FROM cabecImpro WHERE hash = ?`;
      this.db.executeSql(sql, [hash]).then(() => {
        console.log('cabeçalho do improdutivo deletado com sucesso');
        resolve();
      }, (error) => reject(error));
    })
  }

  public deleteCorpImpro(hash: string) {
    return new Promise((resolve, reject) => {
      let sql = `DELETE FROM corpImpro WHERE hash = ?`;
      this.db.executeSql(sql, [hash]).then(() => {
        console.log('corpo do improdutivo deletado com sucesso');
        resolve();
      }, (error) => reject(error));
    })
  }

  public getCabecImpro() {
    return new Promise((resolve, reject) => {

      let sql = `SELECT * FROM cabecImpro`;

      this.db.executeSql(sql, []).then(data => {
        let arrayCabecImpro = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arrayCabecImpro.push({
              hash: data.rows.item(i).hash,
              filial: data.rows.item(i).filial,
              dt_ini: data.rows.item(i).dt_ini,
              dt_final: data.rows.item(i).dt_final,
              hr_ini: data.rows.item(i).hr_ini,
              hr_final: data.rows.item(i).hr_final,
              ccusto: data.rows.item(i).ccusto,
              desccusto: data.rows.item(i).desccusto,
              motivo: data.rows.item(i).motivo,
              descmotivo: data.rows.item(i).descmotivo,
              usuario: data.rows.item(i).usuario,
            });
          }
        }
        resolve(arrayCabecImpro);
      }, (error) => reject(error));
    })
  }

  public getCorpImpro(hash) {
    return new Promise((resolve, reject) => {

      let sql = `SELECT * FROM corpImpro WHERE hash = ?`;// corpImpro WHERE hash = ?`;
      let params = [hash];

      this.db.executeSql(sql, params).then(data => {
        let arrayCorpImpro = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            arrayCorpImpro.push({
              hash: data.rows.item(i).hash,
              codigo: data.rows.item(i).codigo,
              descricao: data.rows.item(i).descricao
            });
          }
        }
        resolve(arrayCorpImpro);
      }, (error) => reject(error));
    })
  }

  public insertCache(codigo_r, codigo_p, ccusto, turno, turma, resultado) {

    return new Promise((resolve, reject) => {
      let sql = `INSERT INTO cache (codigo_r, codigo_p, ccusto, turno, turma, resultado) VALUES (?,?,?,?,?,?)`;
      let params = [
        codigo_r,
        codigo_p,
        ccusto,
        turno,
        turma,
        resultado
      ];

      this.db.executeSql(sql, params).then(() => {
        console.log('cache inserido com sucesso!');
        resolve();
      }, (error) => reject(error));
    });
  }

  public updateCache(codigo_r, codigo_p, ccusto, turno, turma, resultado) {
    return new Promise((resolve, reject) => {
      let sql = `UPDATE cache SET codigo_p = ?, turno = ?, turma = ?, resultado = ? WHERE codigo_r = ? AND ccusto = ?`;
      let params = [codigo_p, turno, turma, resultado, codigo_r, ccusto];
      this.db.executeSql(sql, params).then(() => {
        console.log('cache atualizado com sucesso!');
        resolve();
      }, (error) => reject(error));
    });
  }

  public selectCache(codigo_r, ccusto) {
    return new Promise((resolve, reject) => {

      let sql = `SELECT codigo_p, turno, turma, resultado FROM cache WHERE codigo_r = ? AND ccusto = ?`;

      this.db.executeSql(sql, [codigo_r, ccusto]).then(data => {
        let res: Array<string> = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            /*arrayCache.push({
              id: data.rows.item(i).id,
              codigo_p: data.rows.item(i).codigo_p
            });*/
            res[0] = data.rows.item(i).codigo_p;
            res[1] = data.rows.item(i).turno;
            res[2] = data.rows.item(i).turma;
            res[3] = data.rows.item(i).resultado;
          }
        }
        resolve(res);
      }, (error) => reject(error));
    })
  }

}
